/* Copyright 2015 Pablo Cremades //<>// //<>// //<>//
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**************************************************************************************
 * Autor: Pablo Cremades
 * Fecha: 08/05/2018
 * e-mail: pablocremades@gmail.com
 * Descripción: Frecuencímetro con fotocompuerta
 *
 * Change Log:
 * 
 */

import processing.serial.*;

String[] serialPorts;
Serial port;
String inString = "";
String[] list;
int frecuencia;

void setup() {
  size(400, 300);
  serialPorts = Serial.list(); //Get the list of tty interfaces
  for ( int i=0; i<serialPorts.length; i++) { //Search for ttyACM*
    if ( serialPorts[i].contains("ttyACM") | serialPorts[i].contains("ttyUSB")) {  //If found, try to open port.
      println(serialPorts[i]);
      try {
        port = new Serial(this, serialPorts[i], 115200);
        port.bufferUntil('\n');
      }
      catch(Exception e) {
      }
    }
  }

}

void draw() {
  fill(0);
  if ( port == null ) {  //If failed to open port, print errMsg and exit.
    println("Equipo desconectado. Conéctelo e intente de nuevo.");
    exit();
  }

  //If there is a string available on the port, parse it.
  //Strings not begining with # are data from SAD.
  if (inString.length() > 0 && inString.charAt(0) != '/') {
    try{
      frecuencia = Integer.parseInt(inString);
    }
    catch( NumberFormatException e ){
      frecuencia = 0;
    }
    background(255);
    textSize(40);
    text("Frecuencia: "+frecuencia+" Hz", width/8, height/2);
    inString = ""; //Empty the string.
  }
}

//Read the incoming data from the port.
void serialEvent(Serial port) { 
  inString = port.readString().trim();  //Leemos el string removiendo le line feed
                                        //porque si no el parseInt tiene problemas.
}