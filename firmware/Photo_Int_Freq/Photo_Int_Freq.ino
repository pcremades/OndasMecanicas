double tiempo;

void setup(){
 Serial.begin(115200);
 pinMode(2, INPUT_PULLUP);
 attachInterrupt(INT0, ISR_t, RISING); 
}

void loop(){
}

void ISR_t(){
  tiempo = millis() - tiempo;
  Serial.println(round(1/tiempo*1000));
  tiempo = millis();
}
